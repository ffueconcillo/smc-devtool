FROM smc-dev
WORKDIR /var/www

# configure and run composer
RUN ssh-keyscan -t rsa bitbucket.org >> /root/.ssh/known_hosts
RUN chmod 644 /root/.ssh/known_hosts
RUN composer config --global process-timeout 2000

# install phpunit
RUN wget https://phar.phpunit.de/phpunit-old.phar
RUN chmod +x phpunit-old.phar
RUN mv phpunit-old.phar /usr/local/bin/phpunit

# install xdebug
RUN pecl install -o -f xdebug
COPY xdebug.ini /etc/php.d/xdebug.ini

# set tns names
COPY tnsnames.ora /etc/tnsnames.ora