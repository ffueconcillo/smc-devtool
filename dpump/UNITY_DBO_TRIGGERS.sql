--------------------------------------------------------
--  File created - Wednesday-June-07-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger ACCESS_PERMS_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ACCESS_PERMS_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.access_perms
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' and new.perm_id is null ) begin
if dbms_reputil.replication_is_on = false then
 :new.perm_id := general_seq.nextval;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger access_perms_trig_bi ', iother_data=>'failed insert '||:new.perm_id) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."ACCESS_PERMS_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ACCESS_PERMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ACCESS_PERMS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.access_perms
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger access_perms_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."ACCESS_PERMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ACTION_LOG_ACTION_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ACTION_LOG_ACTION_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.action_log
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.action_id IS NULL THEN
    SELECT group_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(action_id),0) INTO v_newVal FROM action_log;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT group_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.action_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger action_log_action_id_trig_bi', iother_data=>'failed insert'||:new.action_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."ACTION_LOG_ACTION_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ACTION_LOG_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ACTION_LOG_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.action_log
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger action_log_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."ACTION_LOG_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUDIT_LOG_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BI" 
BEFORE INSERT ON unity_dbo.audit_log
FOR EACH ROW
begin
  if (:new.audit_id is null) then
     select unity_dbo.audit_seq.nextval into :new.audit_id from dual;
  end if;
exception
when others then
   dblog.log_to_pipe(ipName=>'Trigger audit_log_trig_bi', iother_data=>'failed insert'||:new.audit_id) ;
   raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUDIT_LOG_TRIG_BI_OLD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BI_OLD" BEFORE INSERT ON unity_dbo.AUDIT_LOG_OLD
    FOR EACH ROW
begin
        if (:new.replication_id is null) then
          select unity_dbo.audit_seq.nextval into :new.replication_id from dual;
        end if;

exception
when others then
         dblog.log_to_pipe(ipName=>'Trigger audit_log_trig_bi', iother_data=>'failed
 insert'||:new.replication_id) ;
                  raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BI_OLD" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUDIT_LOG_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BU" 
BEFORE UPDATE ON unity_dbo.audit_log
FOR EACH ROW
   WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger audit_log_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUDIT_LOG_TRIG_BU_OLD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BU_OLD" BEFORE UPDATE ON unity_dbo.AUDIT_LOG_OLD
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger audit_log_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."AUDIT_LOG_TRIG_BU_OLD" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_ACT_ASSTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_ACT_ASSTS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_action_assets
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_act_assts_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_ACT_ASSTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_ACTIONS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_ACTIONS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_actions
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_actions_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_ACTIONS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_DISCLAIMERS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_DISCLAIMERS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_disclaimers
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_disclaimers_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CGWS_DISCLAIMERS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_FUNC_PHASES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_FUNC_PHASES_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_gws_function_phases
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_func_phases_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_FUNC_PHASES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_FUNC_SUB_TESTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_FUNC_SUB_TESTS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_gws_func_sub_tests
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_func_sub_tests_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_FUNC_SUB_TESTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_FUNC_TEST_LISTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_FUNC_TEST_LISTS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_gws_func_test_lists
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_func_test_lists_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end ;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_FUNC_TEST_LISTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_FUNC_TESTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_FUNC_TESTS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_gws_function_tests
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_func_tests_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_FUNC_TESTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_FUNCS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_FUNCS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_gws_functions
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_funcs_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_FUNCS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_LIST_ITEMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_LIST_ITEMS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_list_items
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_list_items_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CGWS_LIST_ITEMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_LISTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_LISTS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_lists
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_lists_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CGWS_LISTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_PEDITS_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_PEDITS_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.customer_gws_policy_edits
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' and new.change_id is null ) begin
if dbms_reputil.replication_is_on = false then
  :new.change_id := gws_seq.nextval ;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_pedits_trig_bi ', iother_data=>'failed insert '||:new.change_id) ;
    raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_PEDITS_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_PEDITS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_PEDITS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_policy_edits
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_pedits_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_PEDITS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_POLICY_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_POLICY_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_policy
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_policy_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CGWS_POLICY_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_RULES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_RULES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_rules
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_rules_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_RULES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_RULESETS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_RULESETS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_rulesets
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_rulesets_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_RULESETS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_TEMPLATES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_TEMPLATES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_templates
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_templates_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_TEMPLATES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_TEST_LISTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_TEST_LISTS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_test_lists
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_test_lists_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_TEST_LISTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CGWS_TESTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CGWS_TESTS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_tests
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_tests_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CGWS_TESTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CPERMS_ALLOWED_NEW_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CPERMS_ALLOWED_NEW_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_perms_allowed_new
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cperms_allowed_new_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."CPERMS_ALLOWED_NEW_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CROLES_PERMS_NEW_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CROLES_PERMS_NEW_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_role_perms_new
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger croles_perms_new_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
    raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."CROLES_PERMS_NEW_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_ATTR_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_ATTR_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_attributes
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_attr_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_ATTR_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_DICT_ITEMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_DICT_ITEMS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_dictionary_items
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_dict_items_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_DICT_ITEMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_DICTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_DICTS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_dictionaries
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_dicts_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_DICTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_DOMAINS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_DOMAINS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_domains
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_domains_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_DOMAINS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_GWS_SUB_TESTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_GWS_SUB_TESTS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_sub_tests
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cgws_sub_tests_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_GWS_SUB_TESTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_HIST_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_HIST_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_history
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_hist_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_HIST_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_LDAP_HOST_HOST_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_LDAP_HOST_HOST_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.customer_ldap_host
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.host_id IS NULL THEN
    SELECT general_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(host_id),0) INTO v_newVal FROM customer_ldap_host;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT general_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.host_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_ldap_host_host_id_trig_bi', iother_data=>'failed insert'||:new.host_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_LDAP_HOST_HOST_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_LDAP_HOST_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_LDAP_HOST_TRIG_BU" 
    BEFORE UPDATE OF host_id, customer_id, host_type, description, host, port, use_ssl, protocol_version, enable_pwd_chk, login_name, login_password, basedn, query, login_attr, firstname_attr, lastname_attr, password_attr, email_attr, email_alias_attr, group_attr, groupmethod_attr, memberof_attr, relationship_attr, user_filter, group_filter, email_alias_filter, synchost, schedule_sync, retry_times, sched_run_every, sched_run_day, sched_month_day, sched_hour, sched_min, sched_time_set, sync_start_time, last_successful_sync_dt, hybrid_master, managerdn_attr, sync_ous ON unity_dbo.customer_ldap_host
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_ldap_host_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_LDAP_HOST_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_LDAP_HOST_TRIG_BU_CG1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_LDAP_HOST_TRIG_BU_CG1" 
    BEFORE UPDATE OF doing_sync ON unity_dbo.customer_ldap_host
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_cg1_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_ldap_host_trig_bu_cg1', iother_data=>'failed update'||:new.last_update_cg1_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_LDAP_HOST_TRIG_BU_CG1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_MEMBER_MEMBER_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_MEMBER_MEMBER_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.customer_member
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.member_id IS NULL THEN
    SELECT  group_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(member_id),0) INTO v_newVal FROM customer_member;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT group_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.member_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_member_member_id_trig_bi', iother_data=>'failed insert'||:new.member_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_MEMBER_MEMBER_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_MEMBER_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_MEMBER_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_member
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_member_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_MEMBER_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_NEST_LIST_ITEMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_NEST_LIST_ITEMS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.cust_nested_list_items
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_nest_list_items_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."CUST_NEST_LIST_ITEMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_NEST_LISTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_NEST_LISTS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.cust_nested_lists
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_nest_lists_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."CUST_NEST_LISTS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_NEST_SL_ITEMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_NEST_SL_ITEMS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.cust_nested_sublist_items
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_nest_sl_items_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."CUST_NEST_SL_ITEMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_ORDRS_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_ORDRS_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.customer_orders
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.order_id IS NULL THEN
    SELECT orders_seq.NEXTVAL INTO v_newVal FROM DUAL;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.order_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_ordrs_trig_bi', iother_data=>'failed insert'||:new.order_id) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_ORDRS_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_OUS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_OUS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_ous
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_ous_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_OUS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_PACFILE_EXEMPT_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_PACFILE_EXEMPT_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_pacfile_exemption
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_pacfile_exempt_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_PACFILE_EXEMPT_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_PERMS_ALLOWED_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_PERMS_ALLOWED_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_perms_allowed
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_perms_allowed_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_PERMS_ALLOWED_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_RESTRICT_ACCESS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_RESTRICT_ACCESS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_restricted_access
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_restrict_access_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_RESTRICT_ACCESS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_ROLE_DEVS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_ROLE_DEVS_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.customer_role_devices
    FOR EACH ROW
begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_role_devs_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_ROLE_DEVS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_ROLES_PERMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_ROLES_PERMS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_roles_perms
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_roles_perms_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_ROLES_PERMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_ROLES_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_ROLES_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.customer_roles
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' and new.role_id is null ) begin
if dbms_reputil.replication_is_on = false then
 :new.role_id := general_seq.nextval;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_roles_trig_bi ', iother_data=>'failed insert '||:new.role_id) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_ROLES_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_ROLES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_ROLES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_roles
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_roles_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUST_ROLES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUST_SERV_KEY_MAP_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUST_SERV_KEY_MAP_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_service_key_map
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_serv_key_map_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUST_SERV_KEY_MAP_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUSTOMER_GWS_LIST_OVERRIDES_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUSTOMER_GWS_LIST_OVERRIDES_BU" 
    BEFORE UPDATE ON unity_dbo.customer_gws_list_overrides
    FOR EACH ROW
       WHEN (  user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger customer_gws_list_overrides_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."CUSTOMER_GWS_LIST_OVERRIDES_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUSTOMER_INFO_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUSTOMER_INFO_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_info
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger customer_info_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUSTOMER_INFO_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUSTOMER_SERVS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUSTOMER_SERVS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.customer_services
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger customer_servs_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUSTOMER_SERVS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUSTOMERS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUSTOMERS_TRIG_BU" 
    BEFORE UPDATE OF customer_id, company, create_dt, tag_id, partner_id, trusted, industry, locale, timezone ON unity_dbo.customers
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger customers_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUSTOMERS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUSTOMERS_TRIG_BU_CG1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUSTOMERS_TRIG_BU_CG1" 
    BEFORE UPDATE OF status ON unity_dbo.customers
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_cg1_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger customers_trig_bu_cg1', iother_data=>'failed update'||:new.last_update_cg1_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUSTOMERS_TRIG_BU_CG1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CUSTS_CUSTOMER_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."CUSTS_CUSTOMER_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.customers
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.customer_id IS NULL THEN
    SELECT general_seq.NEXTVAL INTO v_newVal FROM DUAL;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.customer_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger custs_customer_id_trig_bi', iother_data=>'failed insert'||:new.customer_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."CUSTS_CUSTOMER_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GEA_GROUP_ALIAS_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GEA_GROUP_ALIAS_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.group_email_alias
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.group_alias_id IS NULL THEN
    SELECT  group_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(group_alias_id),0) INTO v_newVal FROM group_email_alias;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT group_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.group_alias_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger gea_group_alias_id_trig_bi', iother_data=>'failed insert'||:new.group_alias_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GEA_GROUP_ALIAS_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GL_ACL_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GL_ACL_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.group_list_acl
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger grp_list_acl_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GL_ACL_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GL_ADMINS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GL_ADMINS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.group_list_admins
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger gl_admins_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GL_ADMINS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GL_HIERARCHY_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GL_HIERARCHY_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.group_list_hierarchy
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger gl_hierarchy_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GL_HIERARCHY_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GL_INFO_LIST_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GL_INFO_LIST_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.group_list_info
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.list_id IS NULL THEN
    SELECT general_seq.NEXTVAL INTO v_newVal FROM DUAL;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.list_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger gl_info_list_id_trig_bi', iother_data=>'failed insert'||:new.list_id) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."GL_INFO_LIST_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GL_INFO_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GL_INFO_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.group_list_info
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger gl_info_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GL_INFO_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GL_MEMBERS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GL_MEMBERS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.group_list_members
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger gl_members_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GL_MEMBERS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GRP_ALIAS_MBR_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GRP_ALIAS_MBR_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.group_alias_member
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger grp_alias_mbr_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GRP_ALIAS_MBR_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger GRP_EMAIL_ALIAS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."GRP_EMAIL_ALIAS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.group_email_alias
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger grp_email_alias_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."GRP_EMAIL_ALIAS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger HELP_SEARCH_DATA_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."HELP_SEARCH_DATA_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.help_search_data
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger help_search_data_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."HELP_SEARCH_DATA_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger HELP_SEARCH_FILES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."HELP_SEARCH_FILES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.help_search_files
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger help_search_files_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."HELP_SEARCH_FILES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger LDAP_UPDATE_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."LDAP_UPDATE_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.ldap_update
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.seq_id IS NULL THEN
    SELECT ldap_update_seq.NEXTVAL INTO v_newVal FROM DUAL;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.seq_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger ldap_update_id_trig_bi', iother_data=>'failed insert'||:new.seq_id) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."LDAP_UPDATE_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger LDAP_UPDATE_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."LDAP_UPDATE_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.ldap_update
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger ldap_update_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."LDAP_UPDATE_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PART_RESTRICT_ACCESS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PART_RESTRICT_ACCESS_TRIG_BU" 
BEFORE UPDATE ON unity_dbo.partner_restricted_access
FOR EACH ROW
  WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
:new.last_update_ts := systimestamp;
end if;
exception
when others then
dblog.log_to_pipe(ipName=>'Trigger part_restrict_access_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."PART_RESTRICT_ACCESS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PART_SERV_KEY_MAP_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PART_SERV_KEY_MAP_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.partner_service_key_map
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger part_serv_key_map_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PART_SERV_KEY_MAP_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PARTNER_ATTR_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PARTNER_ATTR_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.partner_attributes
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger partner_attr_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PARTNER_ATTR_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PARTNER_CUST_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PARTNER_CUST_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.partner_customization
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger partner_cust_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PARTNER_CUST_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PARTNER_HIST_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PARTNER_HIST_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.partner_history
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger partner_hist_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PARTNER_HIST_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PARTNER_INFO_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PARTNER_INFO_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.partner_info
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger partner_info_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PARTNER_INFO_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PARTNER_SERVICES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PARTNER_SERVICES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.partner_services
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger partner_services_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PARTNER_SERVICES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PARTNERS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PARTNERS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.partners
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger partners_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PARTNERS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PARTS_PARTNER_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PARTS_PARTNER_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.partners
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.partner_id IS NULL THEN
    SELECT general_seq.NEXTVAL INTO v_newVal FROM DUAL;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.partner_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger parts_partner_id_trig_bi', iother_data=>'failed insert'||:new.partner_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PARTS_PARTNER_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PERM_SCOPE_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PERM_SCOPE_TRIG_BU" 
    BEFORE UPDATE ON UNITY_DBO.perm_scope
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger perm_scope_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."PERM_SCOPE_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PERMISSIONS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PERMISSIONS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.permissions
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger permissions_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PERMISSIONS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger POLICY_TAG_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."POLICY_TAG_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.policy_tag
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger policy_tag_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."POLICY_TAG_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PRICEBOOK_PRODS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PRICEBOOK_PRODS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.pricebook_products
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger pricebook_prods_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."PRICEBOOK_PRODS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PRICEBOOKS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PRICEBOOKS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.pricebooks
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger pricebooks_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PRICEBOOKS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PROD_SALES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PROD_SALES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.product_sales
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger prod_sales_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PROD_SALES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PRODUCT_INFO_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."PRODUCT_INFO_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.product_info
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger product_info_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."PRODUCT_INFO_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ROLE_PERMS_NEW_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ROLE_PERMS_NEW_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.role_perms_new
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger role_perms_new_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."ROLE_PERMS_NEW_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ROLE_PERMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ROLE_PERMS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.role_perms
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger role_perms_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."ROLE_PERMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ROLES_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ROLES_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.roles
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' and new.role_id is null ) begin
if dbms_reputil.replication_is_on = false then
 :new.role_id := general_seq.nextval;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger roles_trig_bi ', iother_data=>'failed insert '||:new.role_id) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."ROLES_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ROLES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."ROLES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.roles
    FOR EACH ROW
       WHEN (  user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger roles_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."ROLES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SAML_SSO_CONFIG_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."SAML_SSO_CONFIG_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.saml_sso_config
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger saml_sso_config_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
    raise;
end;

/
ALTER TRIGGER "UNITY_DBO"."SAML_SSO_CONFIG_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SERVICES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."SERVICES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.services
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger services_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."SERVICES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SERVS_SERVICE_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."SERVS_SERVICE_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.services
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.service_id IS NULL THEN
    SELECT general_seq.NEXTVAL INTO v_newVal FROM DUAL;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.service_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger servs_service_id_trig_bi', iother_data=>'failed insert'||:new.service_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."SERVS_SERVICE_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SMC_JOBS_T_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."SMC_JOBS_T_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.smc_jobs_t
    FOR EACH ROW
       WHEN (  user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger smc_jobs_t_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."SMC_JOBS_T_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SMS_POLICY_POLICY_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."SMS_POLICY_POLICY_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.sms_policy
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.policy_id IS NULL THEN
    SELECT  web_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(policy_id),0) INTO v_newVal FROM sms_policy;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT web_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.policy_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger sms_policy_policy_id_trig_bi', iother_data=>'failed insert'||:new.policy_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."SMS_POLICY_POLICY_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SMS_POLICY_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."SMS_POLICY_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.sms_policy
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger sms_policy_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."SMS_POLICY_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SUPPORT_NOTE_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."SUPPORT_NOTE_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.support_note
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger support_note_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."SUPPORT_NOTE_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger UEA_USER_ALIAS_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."UEA_USER_ALIAS_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.user_email_alias
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.user_alias_id IS NULL THEN
    SELECT  group_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(user_alias_id),0) INTO v_newVal FROM user_email_alias;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT group_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.user_alias_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger uea_user_alias_id_trig_bi', iother_data=>'failed insert'||:new.user_alias_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."UEA_USER_ALIAS_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_ACCESS_HIST_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_ACCESS_HIST_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_access_history
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_access_hist_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_ACCESS_HIST_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_ADMIN_PERMS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_ADMIN_PERMS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_admin_perms
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_admin_perms_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."USER_ADMIN_PERMS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_ATTR_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_ATTR_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_attributes
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if DBMS_REPUTIL.REPLICATION_IS_ON = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_attr_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;
/
ALTER TRIGGER "UNITY_DBO"."USER_ATTR_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_AUTH_DEVICES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_AUTH_DEVICES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_authorized_devices
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_auth_devices_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
    raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_AUTH_DEVICES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_CUST_ROLES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_CUST_ROLES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_customer_roles
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_cust_roles_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_CUST_ROLES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_EMAIL_ALIAS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_EMAIL_ALIAS_TRIG_BU" 
    BEFORE UPDATE OF user_alias_id, alias_address, real_address, alias_action ON unity_dbo.user_email_alias
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_email_alias_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."USER_EMAIL_ALIAS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_EMAIL_ALIAS_TRIG_BU_CG1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_EMAIL_ALIAS_TRIG_BU_CG1" 
    BEFORE UPDATE OF domain_id ON unity_dbo.user_email_alias
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_cg1_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_email_alias_trig_bu_cg1', iother_data=>'failed update'||:new.last_update_cg1_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."USER_EMAIL_ALIAS_TRIG_BU_CG1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_HISTORY_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_HISTORY_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_history
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_history_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."USER_HISTORY_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_MANAGER_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_MANAGER_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_manager
    FOR EACH ROW
       WHEN (  user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_manager_trig_bu ', iother_data=>'failed update '||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_MANAGER_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_PW_HIST_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_PW_HIST_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_pw_history
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_pw_hist_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
    raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_PW_HIST_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_PW_UNLOCKS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_PW_UNLOCKS_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_pw_unlocks
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_pw_unlocks_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
    raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_PW_UNLOCKS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_ROLES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_ROLES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.user_roles
    FOR EACH ROW
       WHEN (  user != 'RUN_WITH_NOTRIG'  ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger user_roles_trig_bu ', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_ROLES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_SERV_KEY_MAP_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_SERV_KEY_MAP_TRIG_BU" 
BEFORE UPDATE ON unity_dbo.user_services_key_map
FOR EACH ROW
   WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
:new.last_update_ts := systimestamp;
end if;
exception
when others then
dblog.log_to_pipe(ipName=>'Trigger user_serv_key_map_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."USER_SERV_KEY_MAP_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_SERVICES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USER_SERVICES_TRIG_BU" 
BEFORE UPDATE ON unity_dbo.user_services
FOR EACH ROW
   WHEN ( user != 'RUN_WITH_NOTRIG' ) declare
  v_ts timestamp := systimestamp ;
begin
  if dbms_reputil.replication_is_on = false then
    :new.last_update_ts := v_ts;
    -- The following IF needs to be inside the dbms_reputil call
    -- to prevent no_data_found repln errs on the destination DB
    -- due to differing state_ts values b/w source and dest DBs.
    if :new.state != :old.state then
      :new.state_ts := v_ts ;
    end if ;
  end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger user_services_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USER_SERVICES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USERS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USERS_TRIG_BU" 
    BEFORE UPDATE OF user_id, email, first_name, last_name, passwd, passwd_type, create_dt, sync_status, auth_service, locale, timezone, host_id, admin_type, plain_passwd, notify_passwd, customer_id, partner_id, group_nm, user_type, list_id, alias_id, contact_addr, ou_id ON unity_dbo.users
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger users_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."USERS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USERS_TRIG_BU_CG1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USERS_TRIG_BU_CG1" 
    BEFORE UPDATE OF status ON unity_dbo.users
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_cg1_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger users_trig_bu_cg1', iother_data=>'failed update'||:new.last_update_cg1_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."USERS_TRIG_BU_CG1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USERS_USER_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."USERS_USER_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.users
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.user_id IS NULL THEN
    SELECT general_seq.NEXTVAL INTO v_newVal FROM DUAL;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.user_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger users_user_id_trig_bi', iother_data=>'failed insert'||:new.user_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."USERS_USER_ID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_PAC_OVERRIDES_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_PAC_OVERRIDES_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.web_pac_overrides
    FOR EACH ROW
begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_pac_overrides_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "UNITY_DBO"."WEB_PAC_OVERRIDES_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POL_PORTBLOCK_RULE_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POL_PORTBLOCK_RULE_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.web_policy_port_blocking
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.rule_id IS NULL THEN
    SELECT  web_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(rule_id),0) INTO v_newVal FROM web_policy_port_blocking;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT web_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.rule_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_pol_portblock_rule_trig_bi', iother_data=>'failed insert'||:new.rule_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POL_PORTBLOCK_RULE_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POL_URL_LIST_URLID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POL_URL_LIST_URLID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.web_policy_url_list
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.url_id IS NULL THEN
    SELECT  web_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(url_id),0) INTO v_newVal FROM web_policy_url_list;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT web_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.url_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_pol_url_list_urlid_trig_bi', iother_data=>'failed insert'||:new.url_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POL_URL_LIST_URLID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POLICY_APPL_FILTER_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POLICY_APPL_FILTER_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.web_policy_appl_filter
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_policy_appl_filter_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POLICY_APPL_FILTER_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POLICY_APPL_TYPE_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POLICY_APPL_TYPE_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.web_policy_appl_type
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_policy_appl_type_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POLICY_APPL_TYPE_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POLICY_CAT_ACT_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POLICY_CAT_ACT_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.web_policy_category_action
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_policy_cat_act_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POLICY_CAT_ACT_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POLICY_PORT_BLOCK_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POLICY_PORT_BLOCK_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.web_policy_port_blocking
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_policy_port_blocking_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POLICY_PORT_BLOCK_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POLICY_RULE_TIME_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POLICY_RULE_TIME_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.web_policy_rule_time
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_policy_rule_time_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POLICY_RULE_TIME_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEB_POLICY_URL_LIST_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEB_POLICY_URL_LIST_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.web_policy_url_list
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger web_policy_url_list_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEB_POLICY_URL_LIST_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEBPOL_APPLFLTR_APPLID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEBPOL_APPLFLTR_APPLID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.web_policy_appl_filter
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.appl_id IS NULL THEN
    SELECT  web_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(appl_id),0) INTO v_newVal FROM web_policy_appl_filter;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT web_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.appl_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger webpol_applfltr_applid_trig_bi', iother_data=>'failed insert'||:new.appl_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEBPOL_APPLFLTR_APPLID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WEBPOL_APPLTYPE_APPLID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WEBPOL_APPLTYPE_APPLID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.web_policy_appl_type
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.appl_id IS NULL THEN
    SELECT  web_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(appl_id),0) INTO v_newVal FROM web_policy_appl_type;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT web_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.appl_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger webpol_appltype_applid_trig_bi', iother_data=>'failed insert'||:new.appl_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WEBPOL_APPLTYPE_APPLID_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WORKGROUP_ATTR_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WORKGROUP_ATTR_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.workgroup_attribute
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger workgroup_attr_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WORKGROUP_ATTR_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WORKGROUP_MEMBER_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WORKGROUP_MEMBER_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.workgroup_member
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger workgroup_member_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WORKGROUP_MEMBER_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WORKGROUP_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WORKGROUP_TRIG_BU" 
    BEFORE UPDATE ON unity_dbo.workgroup
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger workgroup_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WORKGROUP_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WRKGRP_WRKGRP_ID_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "UNITY_DBO"."WRKGRP_WRKGRP_ID_TRIG_BI" 
    BEFORE INSERT ON unity_dbo.workgroup
    FOR EACH ROW
DECLARE
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.workgroup_id IS NULL THEN
    SELECT  web_seq.NEXTVAL INTO v_newVal FROM DUAL;
    -- If this is the first time this table have been inserted into (sequence == 1)
    IF v_newVal = 1 THEN
      --get the max indentity value from the table
      SELECT NVL(max(workgroup_id),0) INTO v_newVal FROM workgroup;
      v_newVal := v_newVal + 1;
      --set the sequence to that value
      LOOP
           EXIT WHEN v_incval>=v_newVal;
           SELECT web_seq.nextval INTO v_incval FROM dual;
      END LOOP;
    END IF;
    --used to emulate LAST_INSERT_ID()
    --mysql_utilities.identity := v_newVal;
   -- assign the value from the sequence to emulate the identity column
   :new.workgroup_id := v_newVal;
  END IF;

exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger wrkgrp_wrkgrp_id_trig_bi', iother_data=>'failed insert'||:new.workgroup_id) ;
        raise;
end;



/
ALTER TRIGGER "UNITY_DBO"."WRKGRP_WRKGRP_ID_TRIG_BI" ENABLE;
