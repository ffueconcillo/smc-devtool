--------------------------------------------------------
--  File created - Wednesday-June-07-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger CUST_DEVS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."CUST_DEVS_TRIG_BU" 
    BEFORE UPDATE ON nsc_dbo.customer_devices
    FOR EACH ROW
       WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
 :new.last_update_ts := systimestamp;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger cust_devs_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
        raise;
end;


/
ALTER TRIGGER "NSC_DBO"."CUST_DEVS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_CUST_MAINT_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_CUST_MAINT_TRIG_BI" 
    BEFORE INSERT ON "NSC_DBO"."MSS_CUST_MAINT_TO_DROP"
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.maint_id := nsc_general_seq.nextval;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger mss_cust_maint_trig_bi ', iother_data=>'failed insert '||:new.maint_id) ;
    raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_CUST_MAINT_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_CUST_MAINT_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_CUST_MAINT_TRIG_BU" 
    BEFORE UPDATE ON "NSC_DBO"."MSS_CUST_MAINT_TO_DROP"
    FOR EACH ROW
      WHEN (  user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger mss_cust_maint_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_CUST_MAINT_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_CUSTMAINTMBR_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_CUSTMAINTMBR_TRIG_BU" 
    BEFORE UPDATE ON NSC_DBO.mss_cust_maint_mbr
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger mss_custmaintmbr_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_CUSTMAINTMBR_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_DEVICEMAINTMBR_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_DEVICEMAINTMBR_TRIG_BU" 
    BEFORE UPDATE ON NSC_DBO.mss_device_maint_mbr
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger mss_devicemaintmbr_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_DEVICEMAINTMBR_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_DOC_MGMT_FG_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_DOC_MGMT_FG_TRIG_BI" 
BEFORE INSERT ON NSC_DBO.mss_doc_mgmt_filegroup
FOR EACH ROW
  WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.file_group_id := mss_general_seq.nextval;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger mss_doc_mgmt_fg_trig_bi ', iother_data=>'failed insert '||:new.file_group_id) ;
  raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_DOC_MGMT_FG_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_DOC_MGMT_FG_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_DOC_MGMT_FG_TRIG_BU" 
BEFORE UPDATE ON NSC_DBO.mss_doc_mgmt_filegroup
FOR EACH ROW
  WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger mss_doc_mgmt_fg_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_DOC_MGMT_FG_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_DOC_MGMT_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_DOC_MGMT_TRIG_BI" 
BEFORE INSERT ON NSC_DBO.mss_doc_mgmt
FOR EACH ROW
  WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.file_id := mss_general_seq.nextval;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger mss_doc_mgmt_trig_bi ', iother_data=>'failed insert '||:new.file_id) ;
    raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_DOC_MGMT_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_DOC_MGMT_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_DOC_MGMT_TRIG_BU" 
BEFORE UPDATE ON NSC_DBO.mss_doc_mgmt
FOR EACH ROW
  WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger mss_doc_mgmt_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_DOC_MGMT_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_SCHED_MAINT_TRIG_BI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_SCHED_MAINT_TRIG_BI" 
    BEFORE INSERT ON NSC_DBO.mss_scheduled_maint
    FOR EACH ROW
      WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.maint_id := mss_general_seq.nextval;
end if;
exception
when others then
    dblog.log_to_pipe(ipName=>'Trigger mss_sched_maint_trig_bi ', iother_data=>'failed insert '||:new.maint_id) ;
    raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_SCHED_MAINT_TRIG_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MSS_SCHED_MAINT_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."MSS_SCHED_MAINT_TRIG_BU" 
    BEFORE UPDATE ON NSC_DBO.mss_scheduled_maint
    FOR EACH ROW
      WHEN (  user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
  :new.last_update_ts := systimestamp;
end if;
exception
when others then
  dblog.log_to_pipe(ipName=>'Trigger mss_sched_maint_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
  raise;
end;

/
ALTER TRIGGER "NSC_DBO"."MSS_SCHED_MAINT_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger REPORT_CONFIGS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."REPORT_CONFIGS_TRIG_BU" 
BEFORE UPDATE ON nsc_dbo.report_configs
FOR EACH ROW
   WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
:new.last_update_ts := systimestamp;
end if;
exception
when others then
dblog.log_to_pipe(ipName=>'Trigger report_configs_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
raise;
end;


/
ALTER TRIGGER "NSC_DBO"."REPORT_CONFIGS_TRIG_BU" ENABLE;
--------------------------------------------------------
--  DDL for Trigger REPORT_EMAIL_RCPTS_TRIG_BU
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "NSC_DBO"."REPORT_EMAIL_RCPTS_TRIG_BU" 
BEFORE UPDATE ON nsc_dbo.RER_OLD
FOR EACH ROW
   WHEN ( user != 'RUN_WITH_NOTRIG' ) begin
if dbms_reputil.replication_is_on = false then
:new.last_update_ts := systimestamp;
end if;
exception
when others then
dblog.log_to_pipe(ipName=>'Trigger report_email_rcpts_trig_bu', iother_data=>'failed update'||:new.last_update_ts) ;
raise;
end;


/
ALTER TRIGGER "NSC_DBO"."REPORT_EMAIL_RCPTS_TRIG_BU" ENABLE;
