#!/bin/bash

: ${SMC_CONTAINER_NAME=smc-dev}

usage() {
    printf "USAGE: $0 [arg]
        Set SMC_CONTAINER_NAME env variable to your SMC container name.

        clear  - Clear twig cache. cache:clear
        build - Build front-end asset. asset:install & assetic:dump
        phpunit - Run phpUnit
        l10n - Generate strings
        routes - Generate routes
        composer install - Install PHP dependencies
        composer update - Update PHP dependencies
        \n"
        
}

clearCache() {
   docker exec $SMC_CONTAINER_NAME bash -c "app/console cache:clear"
} 

buildAssets() {
    docker exec $SMC_CONTAINER_NAME bash -c "app/console assets:install && app/console assetic:dump"
}

phpUnit() {
    docker exec $SMC_CONTAINER_NAME bash -c "phpunit --exclude-group=VPC -c app"
}

composerInstall() {
    docker exec $SMC_CONTAINER_NAME bash -c "composer install"
}

composerUpdate() {
    docker exec $SMC_CONTAINER_NAME bash -c "composer update"
}

l10n() {
    docker exec $SMC_CONTAINER_NAME bash -c "app/console assets:translation:install"
}

routes() {
    docker exec $SMC_CONTAINER_NAME bash -c "app/console fos:js-routing:dump"
}

case ${1} in
    "clear")
        clearCache
        ;;
    "build")
        clearCache
        buildAssets
        ;;
    "phpunit")
        phpUnit
        ;;
    "l10n")
        l10n
        ;;
    "routes")
        routes
        ;;
    "composer")
        case ${2} in
            "install")
                composerInstall
                ;;
            "update")
                composerUpdate
                ;;
        esac
        ;;
    *)
        usage
esac
