# SMC-devtool

SMC developer toolbelt

Disclaimer: WORK-IN-PROGRESS

#### Create docker images

##### SMC
Loading smc-dev docker image (make sure docker is running)
- Get the smc-dev.tar image from team member
```
docker load < [smc-dev.tar location]
```

- Create smc-devtool docker image from smc-dev image
```
docker build --tag devtool docker/devtool
```

##### Memcached
- Loading memcached docker image
```
docker pull memcached   (needs login from hub.docker.com)
```

- Start memcached docker container
```
docker run -d --name memcache --restart unless-stopped memcached
```

- Start smc-dev + devtool + memcached image & map current dir to /var/www
- Make sure port 80 is not used in host 
```
cd unityui/web
docker run -d -v $(pwd):/var/www -w /var/www -p 80:80 -l memcache:memcache--name smc --restart unless-stopped devtool
```

##### Oracle DB
[Go here](https://perimeter.jira.com/wiki/spaces/~ffadzil/pages/187171339/Setup+Oracle+Docker+on+Localhost)

#### Install PHP dependencies by composer
```
devtool composer install
```

